use std::fs::File;
use std::io::{self, Write, Read};

fn look_for_thymine(proteins:Vec<char>) -> bool {
    let lenght_data:usize = proteins.len();
    if proteins[lenght_data - 1] == 't'{
        return true
    }
    false
}


fn look_for_guanines(proteins: &Vec<char>) -> bool {
    let mut count:i32 = 0;
    for &protein in proteins.iter() {
        if protein == 'g' {
            count += 1;
            if count >=3 {
                return false;
            }
        } else {
            count = 0;
        }
    }
    true
}


fn look_adedinas_or_cytokines(data: Vec<char>, character: char) -> bool {
    let mut count_iterations:usize  = 0; 
    let mut count_protein: i32 = 0;
    while count_iterations < data.len() {
        if data[count_iterations] == character {
            count_protein += 1;
            if count_protein >= 2 {
                return true;
            }   
        }
        count_iterations += 1;
    }
    false
}


fn is_mutant_or_human(data: Vec<char>) -> (){
    if data.is_empty(){
        println!("It's empty and we can't work with this data.");
    } else if look_for_thymine(data.clone()) == true && look_for_guanines(&data.clone()) && (look_adedinas_or_cytokines(data.clone(), 'a') || look_adedinas_or_cytokines(data.clone(), 'c')) {
        println!("This person is mutant");
    } else {
        println!("This is person is human");
    }
}


fn read_file(name_file: String) -> Vec<char> {
    let mut file: File = File::open(name_file).expect("Error al abrir el archivo");
    let mut content: String = String::new();
    file.read_to_string(&mut content).expect("Error al leer el archivo");
    content.chars().collect()
}


fn main() {
    print!("Enter the name of the file: ");
    io::stdout().flush().unwrap();
    let mut name_file: String = String::new();
    io::stdin().read_line(&mut name_file).expect("Failed to read line");
    let name_file: String = name_file.trim().to_string();
    let path_with_name_file: String = format!("data/{}", name_file);
    is_mutant_or_human(read_file(path_with_name_file));
}
